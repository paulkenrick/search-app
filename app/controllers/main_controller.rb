class MainController < ApplicationController

  def search
    url = 'https://gist.githubusercontent.com/carlos-alberto/c72b9ba94e35447d9ce33b18038b25ad/raw/862d147a8f99c96d8e1dda2531a902947b576719/exercise-data.json'
    uri = URI(url)
    response = Net::HTTP.get(uri)
    @contacts = JSON.parse(response)

    if request.post?
      @searched_term = params[:search][:term].downcase
      @contacts = @contacts.select{ |contact|
        contact.values.join(' ').downcase.include?(@searched_term)
      }
    end
  end

end
