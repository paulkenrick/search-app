require 'rails_helper'

RSpec.describe MainController, type: :controller do

  describe '#search' do
    before(:each) do
      stub_request(:get, 'https://gist.githubusercontent.com/carlos-alberto/c72b9ba94e35447d9ce33b18038b25ad/raw/862d147a8f99c96d8e1dda2531a902947b576719/exercise-data.json').
        to_return(body: '[{"name": "First Person", "email": "first.person@example.com"}, {"name": "Second Person", "email": "second.person@example.com"}]')
    end

    context 'when request is a GET' do
      it 'makes a request to the json endpoint' do
        expect(Net::HTTP).to receive(:get).and_call_original
        get :search
      end

      it 'returns all records' do
          get :search
          expect(assigns(:contacts).length).to eq(2)
          expect(assigns(:contacts)).to include({"name": "First Person", "email": "first.person@example.com"}.stringify_keys)
          expect(assigns(:contacts)).to include({"name": "Second Person", "email": "second.person@example.com"}.stringify_keys)
      end
    end

    context 'when request is a POST' do
      it 'makes are request to the json endpoint' do
        expect(Net::HTTP).to receive(:get).and_call_original
        post :search, params: { search: { term: 'First' } }
      end

      it 'returns records filter based on search term' do
        post :search, params: { search: { term: 'First' } }
        expect(assigns(:contacts).length).to eq(1)
        expect(assigns(:contacts).first).to eq({"name": "First Person", "email": "first.person@example.com"}.stringify_keys)
      end
    end
  end

end
